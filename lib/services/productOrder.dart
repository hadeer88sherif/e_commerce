import 'package:ecommerce/model/product_model.dart';
import 'package:flutter/cupertino.dart';

class ProductOrder with ChangeNotifier{
  List<Product> products =[];
  addProduct(Product product){
    products.add(product);
    notifyListeners();
  }
  deleteProduct(Product product){
    products.remove(product);
    notifyListeners();
  }
}