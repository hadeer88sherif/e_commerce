import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/order_model.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:flutter/cupertino.dart';

class Store with ChangeNotifier {
  final Firestore _fireStore = Firestore.instance;

  addProduct(Product product) {
    _fireStore.collection(kProductsCollection).add({
      kProductName: product.productName,
      kProductDescription: product.productDescription,
      kProductPrice: product.productPrice,
      kProductImage: product.productImage,
      kProductCategory: product.productCategory,
    });
  }

  Future<List<Product>> loadingProduct() async {
    final snapshot =
        await _fireStore.collection(kProductsCollection).getDocuments();
    List<Product> products = [];
    for (var doc in snapshot.documents) {
      var data = doc.data;
      products.add(Product(
        productId: doc.documentID,
        productDescription: data[kProductDescription],
        productName: data[kProductName],
        productImage: data[kProductImage],
        productPrice: data[kProductPrice],
        productCategory: data[kProductCategory],
      ));
    }
    notifyListeners();
    return products;
  }

  editProduct(data, documentId) {
    _fireStore
        .collection(kProductsCollection)
        .document(documentId)
        .updateData(data);
  }

  delete(documentId) {
    _fireStore.collection(kProductsCollection).document(documentId).delete();
  }

  storeOrder(data, List<Product> products) {
    var documentRef = _fireStore.collection(kOrderCategory).document();
    documentRef.setData(data);
    for (var product in products) {
      documentRef.collection(kOrderDetails).document().setData({
        kProductQuantity: product.productQuantity,
        kProductImage: product.productImage,
        kProductPrice: product.productPrice,
        kProductName: product.productName,
      });
    }
  }
  List<Orders> orders = [];
  Future<List<Orders>> loadingOrders() async {
    final snapshot = await _fireStore.collection(kOrderCategory).getDocuments();
    orders = [];
    for (var doc in snapshot.documents) {
      var data = doc.data;
      orders.add(Orders(
          totalPrice: data[kTotalPrice],
          orderAdress: data[kOrderAddress],
          id: doc.documentID));
    }
    notifyListeners();
    return orders;
  }

  Future<List<Product>> OrderDitails(String documentId) async {
    final snapshot = await _fireStore
        .collection(kOrderCategory)
        .document(documentId)
        .collection(kOrderDetails)
        .getDocuments();
    List<Product> orderDetails = [];
    for (var doc in snapshot.documents) {
      var data = doc.data;
      orderDetails.add(Product(
        productName: data[kProductName],
        productQuantity: data[kProductQuantity],
      ));
    }
    notifyListeners();
    return orderDetails;
  }
}
