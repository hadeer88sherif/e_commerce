import 'package:flutter/material.dart';

import '../constant.dart';

class customTextFornField extends StatelessWidget {
  customTextFornField({this.hint, this.iconData,this.onSaved,this.initialvalue});

  final String hint;
  final String initialvalue;
  final IconData iconData;
  final Function onSaved;
   String _errorMessage(){
     switch(hint){
       case "Enter Your Name" : return "Enter Your Name";
       case "Enter Your Password" : return "Enter Your Password";
       case "Enter Your Email" : return "Enter Your Email";
       case "Product Name" : return "Enter Product Name";
       case "Product Description" : return "Enter Product Description";
       case "Product Image" : return "Enter Product Image";
       case "Product Price" : return "Enter Product Price";
     }
   }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: TextFormField(
        initialValue: initialvalue,
        validator: (value) {
          if (value.isEmpty) {
            return _errorMessage();
          }
        },
        cursorColor: kMainColor,
        onSaved: onSaved,
        obscureText: hint=='Enter Your Password' ?true :false,
        decoration: InputDecoration(
          hintText: hint,
          prefixIcon: Icon(
            iconData,
            color: kMainColor,
          ),
          filled: true,
          fillColor: kSecondColor,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: Colors.white)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: Colors.white)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: Colors.white)),
        ),
      ),
    );
  }
}
