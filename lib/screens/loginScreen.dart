import 'package:ecommerce/constant.dart';
import 'package:ecommerce/providers/admin_mode.dart';
import 'package:ecommerce/providers/modal_progress_hud.dart';
import 'package:ecommerce/screens/adminHome.dart';
import 'package:ecommerce/screens/homePage.dart';
import 'package:ecommerce/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../widgets/customTextFormField.dart';
import '../screens/signUp.dart';

class LoginScreen extends StatelessWidget {
  @override
  static String id = "LoginScreen";
  final _auth = Auth();
  String _email;
  String _password;

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
//    bool adminMode = Provider.of<AdminMode>(context).adminMode;
    return Scaffold(
      backgroundColor: kMainColor,
      body: ModalProgressHUD(
        inAsyncCall: Provider.of<Modelprogres>(context).modelValue,
        child: Form(
          key: _globalKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Container(
                  padding: EdgeInsets.only(top: 25),
                  height: MediaQuery.of(context).size.height * .2,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Image.asset("images/icons/logo.png"),
                      Positioned(
                        bottom: 0,
                        child: Text(
                            "Fast Food",
                            style: TextStyle(fontSize: 25,color: kfourthColor),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: height * .1,
              ),
              customTextFornField(
                hint: "Enter Your Email",
                iconData: Icons.email,
                onSaved: (String value) {
                  _email = value;
                },
              ),
              SizedBox(
                height: height * .02,
              ),
              customTextFornField(
                hint: "Enter Your Password",
                iconData: Icons.lock,
                onSaved: (String value) {
                  _password = value;
                },
              ),
              SizedBox(
                height: height * .03,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 120),
                child: Builder(
                  builder: (context) => FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    color: kthirdColor,
                    onPressed: () {
                      login(context);
                    },
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: height * .05,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Don\'t have an accont ?",
                    style: TextStyle(color: kfourthColor, fontSize: 16),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, SignUpScreen.id);
                    },
                    child: Text(
                      "Sign Up",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: height * .05,
              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.center,
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  Expanded(
//                    child: GestureDetector(
//                        onTap: () {
//                          Provider.of<AdminMode>(context, listen: false)
//                              .changeModelValue(true);
//                        },
//                        child: Center(
//                          child: Text(
//                            "I'm Admin",
//                            style: TextStyle(
//                                fontSize: 20,
//                                color: adminMode ? kMainColor : Colors.white),
//                          ),
//                        )),
//                  ),
//                  Expanded(
//                    child: GestureDetector(
//                        onTap: () {
//                          Provider.of<AdminMode>(context, listen: false)
//                              .changeModelValue(false);
//                        },
//                        child: Center(
//                          child: Text(
//                            "I'm User",
//                            style: TextStyle(
//                                fontSize: 20,
//                                color: adminMode ? Colors.white : kMainColor),
//                          ),
//                        )),
//                  ),
//                ],
//              )
            ],
          ),
        ),
      ),
    );
  }

  void login(BuildContext context) async {
    final modelValue = Provider.of<Modelprogres>(context, listen: false);
    if (_globalKey.currentState.validate()) {
      modelValue.changeModelValue(true);
      _globalKey.currentState.save();
      if (_password == "admin12345") {
        try {
          await _auth.SignIn(_email, _password);
          modelValue.changeModelValue(false);
          Navigator.pushReplacementNamed(context, AdminHome.id);
        } catch (e) {
          modelValue.changeModelValue(true);
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(e.message)));
          modelValue.changeModelValue(false);
        }
      } else
        try {
          await _auth.SignIn(_email, _password);
          modelValue.changeModelValue(false);
          Navigator.pushReplacementNamed(context, HomePage.id);
        } catch (e) {
          modelValue.changeModelValue(true);
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(e.message)));
          modelValue.changeModelValue(false);
        }
      modelValue.changeModelValue(false);
    }
  }
}
