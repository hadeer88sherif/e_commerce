import 'package:ecommerce/constant.dart';
import 'package:ecommerce/screens/addProduct.dart';
import 'package:ecommerce/screens/mangeProduct.dart';
import 'package:ecommerce/screens/viewOrder.dart';
import 'package:ecommerce/services/store.dart';
import 'package:ecommerce/widgets/badge.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AdminHome extends StatefulWidget {
  static String id = "AdminHome";

  @override
  _AdminHomeState createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {
  @override
  void didChangeDependencies()async {
    // TODO: implement didChangeDependencies
   await Provider.of<Store>(context).loadingOrders();
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kMainColor,
      appBar: AppBar(
        title: Text("Admin"),
        backgroundColor: kthirdColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50,vertical: 10),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                  color: kthirdColor
                ),
                width: MediaQuery.of(context).size.width,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, AddProduct.id);
                  },
                  child: Text("Add Product",style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50,vertical: 10),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    color: kthirdColor
                ),
                width: MediaQuery.of(context).size.width,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, MangeProduct.id);
                  },
                  child: Text("Mange Product",style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50,vertical: 10),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    color: kthirdColor
                ),
                width: MediaQuery.of(context).size.width,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, ViewOrder.id);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("View order",style: TextStyle(color: Colors.white),),
                      Consumer<Store>(
                        builder: (_, cart, ch) => Badge(
                          child: ch,
                          color: kMainColor,
                          value: cart.orders.length.toString(),
                        ),
                        child: IconButton(
                          color:Colors.black54,
                          iconSize: 30,
                          icon: Icon(
                            Icons.notifications,
                          ),
                          onPressed: () {
                            Navigator.of(context).pushNamed(ViewOrder.id);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

