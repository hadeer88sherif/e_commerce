import 'package:ecommerce/constant.dart';
import 'package:ecommerce/screens/fastfood.dart';
import 'package:ecommerce/screens/orderScreen.dart';
import 'package:ecommerce/screens/seaFood.dart';
import 'package:ecommerce/screens/sweets.dart';
import 'package:ecommerce/services/auth.dart';
import 'package:ecommerce/services/productOrder.dart';
import 'package:ecommerce/widgets/badge.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  static String id = "HomePage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _auth = Auth();
  FirebaseUser _firebaseUser;
  int _bottomBarIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
//          bottomNavigationBar: BottomNavigationBar(
//              fixedColor: kMainColor,
//              backgroundColor: kthirdColor,
//              currentIndex: _bottomBarIndex,
//              onTap: (value) {
//                setState(() {
//                  _bottomBarIndex = value;
//                });
//              },
//              items: [
//                BottomNavigationBarItem(
//                    icon: Icon(Icons.mood),
//                    title: Text("test")
//                ),
//                BottomNavigationBarItem(
//                    icon: Icon(Icons.mood),
//                    title: Text("test")
//                ),
//                BottomNavigationBarItem(
//                    icon: Icon(Icons.mood),
//                    title: Text("test")
//                ),
//              ]),
          appBar: AppBar(
            actions: <Widget>[
              Consumer<ProductOrder>(
                builder: (_, cart, ch) => Badge(
                  child: ch,
                  value: cart.products.length.toString(),
                ),
                child: IconButton(
                  icon: Icon(
                    Icons.shopping_cart,
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(OrderScreen.id);
                  },
                ),
              ),
            ],
            backgroundColor: kthirdColor,
            title: Text("Fast Food"),
            bottom: TabBar(tabs: <Widget>[
              Tab(
                text: "Sweets",
              ),
              Tab(
                text: "Fast Food",
              ),
              Tab(
                text: "Sea Food",
              )
            ]),
          ),
          body: TabBarView(
            children: <Widget>[
              Sweets(),
              FastFood(),
              SeaFood(),
            ],
          )),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    getCurrentUser();
    super.initState();
  }

  void getCurrentUser() async {
    _firebaseUser = await _auth.getUser();
  }
}
