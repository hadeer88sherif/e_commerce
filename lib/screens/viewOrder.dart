import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/order_model.dart';
import 'package:ecommerce/screens/orderDetails.dart';
import 'package:ecommerce/services/store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ViewOrder extends StatefulWidget {
  static String id = "viewOrder";

  @override
  _ViewOrderState createState() => _ViewOrderState();
}

class _ViewOrderState extends State<ViewOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Orders"),
        backgroundColor: kthirdColor,
      ),
      body: FutureBuilder<List<Orders>>(
          future: Provider.of<Store>(context).loadingOrders(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, i) {
                    return Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            Navigator.pushNamed(context, OrderDetails.id,arguments: snapshot.data[i].id);
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                            child: Container(
                              height: MediaQuery.of(context).size.height*.1,
                              width: MediaQuery.of(context).size.width,
                              decoration:  BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color:  kMainColor
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Total Price \$${snapshot.data[i].totalPrice.toString()}",style: TextStyle(
                                    fontSize: 18
                                  ),),
                                  Text("Order Address ${snapshot.data[i].orderAdress}",style: TextStyle(
                                      fontSize: 18
                                  ),),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  });
            } else {
              return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: kSecondColor,
                  ));
            }
          }),
    );
  }
}
