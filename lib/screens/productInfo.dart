import 'dart:io';

import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/services/productOrder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductInfo extends StatefulWidget {
  static String id = "productInfo";

  @override
  _ProductInfoState createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  int quntity = 1;
  bool exist = false;

  @override
  Widget build(BuildContext context) {
    final Product product = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          title: Text("Product Details"),
          backgroundColor: kthirdColor,
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .53,
                color: kMainColor,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * .4,
                        child: Image(
                          fit: BoxFit.fill,
                          image: AssetImage(product.productImage),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 70,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              product.productDescription,
                              style: TextStyle(
                                  fontSize: 25,
                                  color: kfourthColor,
                                  fontWeight: FontWeight.w700),
                            ),
                            Text(
                              "\$${product.productPrice}",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: kfourthColor,
                                  fontWeight: FontWeight.w700),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: kSecondColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50)),
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        "Quantity",
                        style: TextStyle(
                            fontSize: 35,
                            color: kMainColor,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 70, vertical: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RawMaterialButton(
                            onPressed: () {
                              setState(() {
                                if(quntity>1){
                                  quntity--;
                                }

                              });
                            },
                            elevation: 2.0,
                            fillColor: kMainColor,
                            child: Icon(
                              Icons.minimize,
                              size: 20.0,
                            ),
                            padding: EdgeInsets.all(15.0),
                            shape: CircleBorder(),
                          ),
                          Text(
                            quntity.toString(),
                            style: TextStyle(
                                fontSize: 35,
                                color: kMainColor,
                                fontWeight: FontWeight.w700),
                          ),
                          RawMaterialButton(
                            onPressed: () {
                              setState(() {
                                quntity++;
                              });
                            },
                            elevation: 2.0,
                            fillColor: kMainColor,
                            child: Icon(
                              Icons.add,
                              size: 20.0,
                            ),
                            padding: EdgeInsets.all(15.0),
                            shape: CircleBorder(),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 120),
                      child: Builder(
                        builder: (context) => FlatButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          color: kthirdColor,
                          onPressed: () {
                            ProductOrder order = Provider.of<ProductOrder>(
                                context,
                                listen: false);
                            var productCart = order.products;
                            for (var products in productCart) {
                              if (products.productName == product.productName) {
                                exist = true;
                              } else {
                                exist = false;
                              }
                            }
                            if (exist) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text("You've added this order before"),
                              ));
                            } else {
                              product.productQuantity = quntity;
                              order.addProduct(product);
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text("This order added"),
                              ));
                            }
                          },
                          child: Text(
                            "ADD TO CART",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
