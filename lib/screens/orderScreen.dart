import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/services/productOrder.dart';
import 'package:ecommerce/services/store.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  static String id = "orderScreen";

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  Widget build(BuildContext context) {
    List<Product> orderedProduct = Provider.of<ProductOrder>(context).products;
    return Scaffold(
        backgroundColor: kMainColor,
        appBar: AppBar(
          title: Text("Product Orders"),
          backgroundColor: kthirdColor,
        ),
        body: Column(
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(15),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Total',
                      style: TextStyle(fontSize: 20, color: kthirdColor),
                    ),
                    Spacer(),
                    Chip(
                      label: Text(
                        '\$${price(orderedProduct).toStringAsFixed(2)}',
                        style: TextStyle(
                          color: Theme.of(context).primaryTextTheme.title.color,
                        ),
                      ),
                      backgroundColor: kthirdColor,
                    ),
                    FlatButton(
                        onPressed: () {
                          _showMyDialog(context, orderedProduct);
                        },
                        child: Text(
                          "Order Now",
                          style: TextStyle(color: kthirdColor),
                        ))
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: orderedProduct.length,
                  itemBuilder: (context, i) {
                    return Dismissible(
                      key: ValueKey(orderedProduct[i].productId),
                      background: Container(
                        color: Theme.of(context).errorColor,
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                          size: 40,
                        ),
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 20),
                        margin: EdgeInsets.symmetric(
                          horizontal: 15,
                          vertical: 4,
                        ),
                      ),
                      direction: DismissDirection.endToStart,
                      confirmDismiss: (direction) {
                        return showDialog(
                          context: context,
                          builder: (ctx) => AlertDialog(
                            title: Text(
                              'Are you sure?',
                            ),
                            content: Text(
                              'Do you want to remove the item from the cart?',
                            ),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(
                                  'No',
                                ),
                                onPressed: () {
                                  Navigator.of(ctx).pop();
                                },
                              ),
                              FlatButton(
                                child: Text(
                                  'Yes',
                                ),
                                onPressed: () {
                                  Provider.of<ProductOrder>(context,
                                          listen: false)
                                      .deleteProduct(orderedProduct[i]);
                                  Navigator.of(ctx).pop();
                                },
                              ),
                            ],
                          ),
                        );
                      },
                      onDismissed: (direction) {
                        Provider.of<ProductOrder>(context, listen: false)
                            .deleteProduct(orderedProduct[i]);
                      },
                      child: Card(
                        margin: EdgeInsets.symmetric(
                          horizontal: 15,
                          vertical: 4,
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(8),
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundColor: kMainColor,
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: FittedBox(
                                    child: Image(
                                  image: AssetImage(
                                      orderedProduct[i].productImage),
                                )),
                              ),
                            ),
                            title: Text(orderedProduct[i].productName),
                            subtitle: Text(
                                'Total: \$${int.parse(orderedProduct[i].productPrice) * orderedProduct[i].productQuantity}'),
                            trailing:
                                Text('${orderedProduct[i].productQuantity} x'),
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ));
  }

  double price(List<Product> product) {
    double price = 0;
    for (var products in product) {
      price += int.parse(products.productPrice) * products.productQuantity;
    }
    return price;
  }

  Future<void> _showMyDialog(
      BuildContext context, List<Product> product) async {
    List<Product> products = product;
    String location;
    double totalPrice = price(products);
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Total Price    \$${totalPrice}',
            style: TextStyle(color: kthirdColor),
          ),
          content: SingleChildScrollView(
              child: TextFormField(
                  onChanged: (String value) {
                    setState(() {
                      location = value;
                    });
                  },
                  decoration: InputDecoration(
                    hintText: "Enter Your Location",
                  ))),
          actions: <Widget>[
            FlatButton(
                child: Text(
                  'Order Now',
                  style: TextStyle(color: kthirdColor),
                ),
                onPressed: () {
                  if (totalPrice != null && location != null) {
                    Provider.of<Store>(context, listen: false).storeOrder({
                      kOrderAddress: location,
                      kTotalPrice: totalPrice,
                    }, products);
                    print(location);
                    Navigator.of(context).pop();
                  }
                }),
          ],
        );
      },
    );
  }
}
