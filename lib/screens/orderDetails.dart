import 'package:ecommerce/model/order_model.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/services/store.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constant.dart';

class OrderDetails extends StatefulWidget {
  static String id = "orderDetails";

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    String orderId = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("Order Details"),
        backgroundColor: kthirdColor,
      ),
      body: FutureBuilder<List<Product>>(
          future: Provider.of<Store>(context).OrderDitails(orderId),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, i) {
                    return Column(
                      children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            child: Container(
                              height: MediaQuery.of(context).size.height * .1,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: kMainColor),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Order Name: ${snapshot.data[i].productName.toString()}",
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  Text(
                                    "Quantity: ${snapshot.data[i].productQuantity}",
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ],
                              ),
                            ),
                          ),
                      ],
                    );
                  });
            } else {
              return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: kSecondColor,
                  ));
            }
          }),
    );
  }
}
