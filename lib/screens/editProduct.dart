import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/services/store.dart';
import 'package:ecommerce/widgets/customTextFormField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditProduct extends StatelessWidget {
  static String id = "EditProduct";
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final Product product = ModalRoute.of(context).settings.arguments;
    String productName;
    String productPrice;
    String productDescription;
    String productImage;
    String productCategory;
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: kMainColor,
      body: Form(
        key: _globalKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            customTextFornField(
              hint: "Product Name",
              initialvalue: product.productName,
              onSaved: (String value) {
                productName = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              hint: "Product Price",
              initialvalue: product.productPrice,
              onSaved: (String value) {
                productPrice = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              initialvalue: product.productDescription,
              hint: "Product Description",
              onSaved: (String value) {
                productDescription = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              initialvalue: product.productImage,
              hint: "Product Image",
              onSaved: (String value) {
                productImage = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              initialvalue: product.productCategory,
              hint: "Product Category",
              onSaved: (String value) {
                productCategory = value;
              },
            ),
            SizedBox(
              height: height * .04,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 70),
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    color: kthirdColor
                ),
                child: FlatButton(
                  child: Text(
                    "Edit Product",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_globalKey.currentState.validate()) {
                      _globalKey.currentState.save();
                      print(productName);
                      print(productDescription);
                      print(productImage);
                      Provider.of<Store>(context, listen: false).editProduct({
                        kProductName: productName,
                        kProductDescription: productDescription,
                        kProductPrice: productPrice,
                        kProductImage: productImage,
                        kProductCategory:productCategory,
                      }, product.productId);
                      Navigator.pop(context);
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
