import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/screens/orderScreen.dart';
import 'package:ecommerce/screens/productInfo.dart';
import 'package:ecommerce/services/store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Sweets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    List<Product> _products = [];
    return Scaffold(
      backgroundColor: kMainColor,
      body: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: FutureBuilder<List<Product>>(
            future: Provider.of<Store>(context).loadingProduct(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                _products = [...snapshot.data];
                snapshot.data.clear();
                for (var product in _products) {
                  if (product.productCategory == kSweets) {
                    snapshot.data.add(product);
                  }
                }
                return GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, i) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: GridTile(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushNamed(ProductInfo.id,
                                    arguments: snapshot.data[i]);
                              },
                              child: Image(
                                image: AssetImage(
                                  snapshot.data[i].productImage,
                                ),
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                            footer: GridTileBar(
                              backgroundColor: Colors.black54,
                              title: Text(
                                snapshot.data[i].productName,
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 16, color: kMainColor),
                              ),
                              trailing: IconButton(
                                icon: Icon(
                                  Icons.shopping_cart,
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, ProductInfo.id,
                                      arguments: snapshot.data[i]);
                                },
                                color: kthirdColor,
                              ),
                            ),
                          ),
                        ),
                      );
                    });
              } else
                return Center(
                    child: CircularProgressIndicator(
                  backgroundColor: kSecondColor,
                ));
            }),
      ),
    );
  }
}
