import 'dart:io';
import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/services/store.dart';
import 'package:ecommerce/widgets/customTextFormField.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';


class AddProduct extends StatefulWidget {
  static String id = "AddProduct";

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();
  var _category = [kSweets, kFastFood, kSeaFood];
  var _categorySelected = kSweets;
  @override
  Widget build(BuildContext context) {
    String productName;
    String productPrice;
    String productDescription;
    String productImage;

    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: kMainColor,
      body: Form(
        key: _globalKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            customTextFornField(
              hint: "Product Name",
              onSaved: (String value) {
                productName = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              hint: "Product Price",
              onSaved: (String value) {
                productPrice = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              hint: "Product Description",
              onSaved: (String value) {
                productDescription = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            customTextFornField(
              hint: "Product Image",
              onSaved: (String value) {
                productImage = value;
              },
            ),
            SizedBox(
              height: height * .02,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Choose Category",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  DropdownButton<String>(
                    items: _category.map((String dropDownStringItem) {
                      return DropdownMenuItem<String>(
                        value: dropDownStringItem,
                        child: Text(
                          dropDownStringItem,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      );
                    }).toList(),
                    onChanged: (String newValueSelected) {
                      setState(() {
                        this._categorySelected = newValueSelected;
                      });
                    },
                    value: _categorySelected,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: height * .04,
            ),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 70),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: kthirdColor),
                    child: FlatButton(
                      child: Text(
                        "Add Product",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if (_globalKey.currentState.validate()) {
                          _globalKey.currentState.save();
                          print(_categorySelected);
                          Provider.of<Store>(context, listen: false)
                              .addProduct(Product(
                            productName: productName,
                            productDescription: productDescription,
                            productPrice: productPrice,
                            productImage: productImage,
                            productCategory: _categorySelected,
                          ));
                          Navigator.pop(context);
                        }
                      },
                    )))
          ],
        ),
      ),
    );
  }

}
