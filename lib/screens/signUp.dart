import 'package:ecommerce/constant.dart';
import 'package:ecommerce/providers/modal_progress_hud.dart';
import 'package:ecommerce/screens/loginScreen.dart';
import 'package:ecommerce/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../widgets/customTextFormField.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class SignUpScreen extends StatelessWidget {
  @override
  static String id = "SignUpScreen";
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();
  final _auth = Auth();
  String _userName;
  String _email;
  String _password;

  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: kMainColor,
      body: ModalProgressHUD(
        inAsyncCall: Provider.of<Modelprogres>(context).modelValue,
        child: Form(
          key: _globalKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Container(
                  padding: EdgeInsets.only(top: 25),
                  height: MediaQuery.of(context).size.height * .2,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Image.asset("images/icons/logo.png"),
                      Positioned(
                        bottom: 0,
                        child: Text(
                          "Fast Food",
                          style: TextStyle(fontSize: 25,color: kfourthColor),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: height * .1,
              ),
              customTextFornField(
                hint: "Enter Your Name",
                iconData: Icons.email,
                onSaved: (String value) {
                  _userName = value;
                },
              ),
              SizedBox(
                height: height * .02,
              ),
              customTextFornField(
                hint: "Enter Your Email",
                iconData: Icons.email,
                onSaved: (String value) {
                  _email = value;
                },
              ),
              SizedBox(
                height: height * .02,
              ),
              customTextFornField(
                hint: "Enter Your Password",
                iconData: Icons.email,
                onSaved: (String value) {
                  _password = value;
                },
              ),
              SizedBox(
                height: height * .03,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 120),
                child: Builder(
                  builder: (context) => FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    color: kthirdColor,
                    onPressed: () {
                      signUp(context);
                    },
                    child: Text(
                      "Sign Up",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: height * .05,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Have an accont ?",
                    style: TextStyle(color: kfourthColor, fontSize: 16),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, LoginScreen.id);
                    },
                    child: Text(
                      "Login",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void signUp(BuildContext context) async {
    final modelValue = Provider.of<Modelprogres>(context, listen: false);

    if (_globalKey.currentState.validate()) {
      modelValue.changeModelValue(true);
      _globalKey.currentState.save();
      try {
        await _auth.SignUp(_email.trim(), _password.trim());
        modelValue.changeModelValue(false);
      } catch (e) {
        modelValue.changeModelValue(true);
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(e.message)));
        modelValue.changeModelValue(false);
      }
      modelValue.changeModelValue(false);
    }
  }
}
