import 'package:ecommerce/constant.dart';
import 'package:ecommerce/model/product_model.dart';
import 'package:ecommerce/screens/editProduct.dart';
import 'package:ecommerce/services/store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MangeProduct extends StatefulWidget {
  static String id = "mangeProduct";

  @override
  _MangeProductState createState() => _MangeProductState();
}

class _MangeProductState extends State<MangeProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kthirdColor,
        title: Text("Mange Product"),
      ),
      backgroundColor: kMainColor,
      body: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: FutureBuilder<List<Product>>(
            future: Provider.of<Store>(context).loadingProduct(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, i) {
                      return GestureDetector(
                        onTapUp: (details) {
                          double dx = details.globalPosition.dx;
                          double dy = details.globalPosition.dy;
                          double dx2 = MediaQuery.of(context).size.width - dx;
                          double dy2 = MediaQuery.of(context).size.width - dy;
                          showMenu(
                              context: context,
                              position: RelativeRect.fromLTRB(dx, dy, dx2, dy2),
                              items: [
                                MyPopUpMenuItem(
                                  child: Text("Edit"),
                                  onClick: () {
                                    Navigator.pushNamed(context, EditProduct.id,
                                        arguments: snapshot.data[i]);
                                  },
                                ),
                                MyPopUpMenuItem(
                                  child: Text("Delete"),
                                  onClick: () {
                                    Provider.of<Store>(context, listen: false)
                                        .delete(snapshot.data[i].productId);
                                    Navigator.pop(context);
                                  },
                                ),
                              ]);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                  child: Image(
                                      fit: BoxFit.fill,
                                      image: AssetImage(
                                          snapshot.data[i].productImage))),
                              Positioned(
                                bottom: 0,
                                child: Opacity(
                                  opacity: .7,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(topRight: Radius.circular(50)),
                                      color: Colors.black
                                    ),
                                    width: MediaQuery.of(context).size.width/2.3,
                                    height: MediaQuery.of(context).size.height *
                                        .07,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          child: Text(
                                            snapshot.data[i].productName,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16),
                                          ),
                                        ),
                                        Text(
                                          "\$${snapshot.data[i].productPrice}",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    });
              } else
                return Center(child: Text("Loading..."));
            }),
      ),
    );
  }
}

class MyPopUpMenuItem<T> extends PopupMenuItem<T> {
  final Widget child;
  final Function onClick;

  MyPopUpMenuItem({@required this.child, this.onClick}) : super(child: child);

  @override
  PopupMenuItemState<T, PopupMenuItem<T>> createState() {
    return MyPopUpMenuItemState();
  }
}

class MyPopUpMenuItemState<T, PopMenuItem>
    extends PopupMenuItemState<T, MyPopUpMenuItem<T>> {
  @override
  void handleTap() {
    widget.onClick();
  }
}
