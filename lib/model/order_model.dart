class Orders {
  final String orderAdress;
  final double totalPrice;
  final String id;

  Orders({
    this.id,
    this.totalPrice,
    this.orderAdress,
  });
}
