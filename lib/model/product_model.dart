class Product {
  final String productName;
  final String productPrice;
  final String productDescription;
  final String productImage;
  final String productId;
   int productQuantity;
  final String productCategory;

  Product(
      {this.productQuantity,
      this.productId,
      this.productCategory,
      this.productName,
      this.productPrice,
      this.productDescription,
      this.productImage});
}
