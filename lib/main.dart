import 'package:ecommerce/providers/admin_mode.dart';
import 'package:ecommerce/providers/modal_progress_hud.dart';
import 'package:ecommerce/screens/addProduct.dart';
import 'package:ecommerce/screens/adminHome.dart';
import 'package:ecommerce/screens/editProduct.dart';
import 'package:ecommerce/screens/homePage.dart';
import 'package:ecommerce/screens/loginScreen.dart';
import 'package:ecommerce/screens/mangeProduct.dart';
import 'package:ecommerce/screens/orderDetails.dart';
import 'package:ecommerce/screens/orderScreen.dart';
import 'package:ecommerce/screens/productInfo.dart';
import 'package:ecommerce/screens/viewOrder.dart';
import 'package:ecommerce/services/productOrder.dart';
import 'package:ecommerce/services/store.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce/screens/signUp.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Modelprogres()),
        ChangeNotifierProvider.value(value: AdminMode()),
        ChangeNotifierProvider.value(value: Store()),
        ChangeNotifierProvider.value(value: ProductOrder()),
      ],
      child: MaterialApp(
        initialRoute: LoginScreen.id,
        routes: {
          LoginScreen.id: (context) => LoginScreen(),
          SignUpScreen.id: (context) => SignUpScreen(),
          AdminHome.id: (context) => AdminHome(),
          AddProduct.id: (context) => AddProduct(),
          MangeProduct.id: (context) => MangeProduct(),
          EditProduct.id: (context) => EditProduct(),
          HomePage.id: (context) => HomePage(),
          ProductInfo.id: (context) => ProductInfo(),
          OrderScreen.id: (context) => OrderScreen(),
          ViewOrder.id: (context) => ViewOrder(),
          OrderDetails.id: (context) => OrderDetails(),
        },
      ),
    );
  }
}
