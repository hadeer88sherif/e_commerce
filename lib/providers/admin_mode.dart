import 'package:flutter/cupertino.dart';

class AdminMode with ChangeNotifier{
  bool adminMode=false;
  void changeModelValue(bool value){
    adminMode=value;
    notifyListeners();
  }
}